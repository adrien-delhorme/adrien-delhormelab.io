# Blog

## Install
```
python3 -m virtualenv .venv
source .venv/bin/activate
python3 -m pip install -r requirements.txt
```

## Development server
```
make devserver
```
## Generate responsive webp images
```
make images
```
