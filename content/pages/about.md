Title: About

## Me

Hello, I'm Adrien.

## Credits

- Font: [IBM Plex](https://www.ibm.com/plex/)
- Icons:
    - External link icon: by [Denelson83](https://commons.wikimedia.org/w/index.php?curid=849803) - Own work, Public Domain
    - Translate icon by Lion Kuil from [TheNounProject.com](https://thenounproject.com)
- Website's logo and favicon are made with [Inkscape](https://inkscape.org)
- Website generated with [Pelican](https://blog.getpelican.com/)
