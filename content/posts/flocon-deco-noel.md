Title: Décoration de Noël : flocon en bois en 3D
Summary: Les plans pour faire des flocons en bois 3D à la découpeuse laser
Date: 2024-12-23
Category: DIY
Tags: DIY, Bois, Bricolage
Slug: flocon-deco-noel
Id: christmas-flake-decoration
Lang: fr

{% from "partials/img.html" import responsive %}

<div class="deux-colonnes">
    {{ responsive("/assets/flocon-deco-noel/", "flocon-deco-noel-bois-1", alt="Décoration flocon de Noël 3D en bois") }}
    {{ responsive("/assets/flocon-deco-noel/", "flocon-deco-noel-bois-2", alt="Décoration flocon de Noël 3D en bois") }}
</div>

Lors de mon passage au [8fablab](https://8fablab.fr/ "Voir le site du 8fablab"), jʼai profité de la découpeuse laser pour faire quelques
décorations pour le sapin de Noël (oui [encore...](etoile-deco-noel "Voir lʼarticle sur lʼétoile de Noël")).

On trouve beaucoup de flocons découpés au laser sur internet, mais je voulais faire des flocons en 3D et non pas plats.

Jʼai donc créé [mon fichier pour la découpe](/assets/flocon-deco-noel/flocons.cut.svg) avec deux modèles de flocon en 2D et les éléments
pour créer les branches de ces flocons, tournées à 90° par rapport aux autres.
Les formes sont très petites et après assemblage à la colle à bois, le tout semble un peu fragile.

Mais le résultat est satisfaisant, et avec un peu de peinture pour masquer les bords brûlés par le laser, le rendu est sympa.

<img src="/assets/flocon-deco-noel/flocons.svg" title="Les formes pour la découpe" />
[Télécharger le fichier SVG pour la découpe](/assets/flocon-deco-noel/flocons.cut.svg)

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank"><img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" alt="CC BY-NC-SA 4.0" width="117" height="41" /></a>