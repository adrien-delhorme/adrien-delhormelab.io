Title: Puzzle Roads v2
Summary: The improved version of the roads to assemble for children to play with toy cars.
Date: 2024-05-03
Category: DIY
Tags: DIY, Majorette, Toy, Car, Road, Puzzle
Slug: puzzle-roads-v2
Id: puzzle-roads-v2
Lang: en

{% from "partials/img.html" import responsive %}

[With the first results being conclusive](/puzzle-roads-v1.html), I am launching an improved version of my roads.

## The New Specifications

- Road tiles to assemble to create a circuit or network
- 🆕 The elements hold together thanks to hermaphroditic tabs (male and female at the same time), which do not impose a direction on the tile
- Road width suitable for 1/64 scale miniature vehicles
- True to reality and its diversity
- Allowing complex assemblies (intersections, turns, etc.)
- Constructible with accessible materials and open-source software
- Durable, bio-sourced, and as recyclable as possible
- 🆕 The cutting of the elements does not follow the hexagonal shape but follows the shape of the road

## Realization

The steps are (almost) the same as for [v1](/puzzle-raods-v1.html). The differences are as follows:

### Hermaphroditic Tabs

<img src="/assets/routes-puzzle-v2/types-ergots.svg" />

For the tiles to hold together, I need tabs like those on puzzle pieces, for example. But there are two major drawbacks to this male/female system of puzzle pieces:

1. You cannot rotate a tile to use it in the other direction: male/male or female/female does not work.
2. If you introduce "branch" tiles, you will probably end up wanting to connect two males or two females: a male/male or female/female adapter will be necessary.

However, an advantage is that you can flip (front/back) the tiles to change the direction of a turn, for example, but in this case, it must be printed on both sides: this is the solution chosen by Waytoplay, Brio, and almost all other competitors.

From experience with Brio tracks, M/M or F/F adapters are a constraint, and I do not intend to print my support on both sides: I naturally choose hermaphroditic tabs, which are male and female at the same time.

### Laser Cutting

Living not far from a very good [fablab](https://8fablab.fr), I will take my license on the Laser System 1490 cutter/engraver. This makes the cutting of the tabs feasible, and I also take the opportunity to cut the tiles to the shape of the printed roads rather than keeping the "blanks."

{{ responsive("/assets/routes-puzzle-v2/", "decoupeuse-laser", alt="The roads being cut in the Laser System 1490 of the fablab") }}

### Improvement of Road Traces

I take the opportunity to improve the tight turn and parking tiles. I add "chicane" tiles and abandon the "bridge" tile. The source files for printing and cutting are free and downloadable right here:

<div class="deux-colonnes">
    <ul>
        <li><a href="/assets/routes-puzzle-v2/impression.pdf" title="Download impression.pdf">impression.pdf</a></li>
        <li><a href="/assets/routes-puzzle-v2/impression.source.svg" title="Download impression.source.svg">impression.source.svg</a></li>
        <li><a href="/assets/routes-puzzle-v2/découpe.svg" title="Download découpe.svg">découpe.svg</a></li>
    </ul>
    <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank"><img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" alt="CC BY-NC-SA 4.0" width="117" height="41" /></a>
</div>

## The Result

The result is even better. The tiles hold together perfectly but remain very easy to assemble/separate. The close-cut tracing gives a Waytoplay-like appearance.

{{ responsive("/assets/routes-puzzle-v2/", "routes-puzzle-v2", alt="Completed Puzzle Roads v2") }}

But there are (inevitably) new problems:

- Calibrating the laser cutter was laborious, and the cutting shifted from the printing trace at the ends.
- The numerous "protruding" shapes of the tabs promote the detachment of the printed sheet: the spray glue is not strong enough.
- Laser cutting induces a strong burnt wood smell, not very pleasant, even if it fades over time.

Maybe I will improve these points in v3!
