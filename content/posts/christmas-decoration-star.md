Title: Christmas Decoration: Wooden Star
Summary: Plans for cutting a wooden star with a scroll saw
Date: 2023-01-03
Category: DIY
Tags: DIY, Wood, Craft
Slug: christmas-star-decoration
Id: christmas-star-decoration
Lang: en

{% from "partials/img.html" import responsive %}

{{ responsive("/assets/etoile-deco-noel/", "etoile-deco-noel-bois", alt="Christmas wooden star decoration") }}

Every year-end, we have the habit of making new decorations for the Christmas tree. So, I went looking on the internet for inspiration to create something beautiful, preferably in wood, to give my scroll saw a bit of a workout.

During an image search, I came across a photo of a star that really caught my eye, and I decided to reproduce it.

I'm sharing here the SVG plans that I drew with <a href="https://inkscape.org/fr/" title="Visit Inkscape's website">Inkscape</a>, which are easily adaptable (shapes, dimensions):

<ul>
  <li><a href="/assets/etoile-deco-noel/plan-etoile-bois.pdf" title="Download the PDF plan of the wooden star">PDF Plan of the Wooden Star</a></li>
  <li><a href="/assets/etoile-deco-noel/source-plan-etoile-bois.svg" title="Download the SVG source of the wooden star plan">SVG Source of the Wooden Star Plan</a></li>
</ul>

{{ responsive("/assets/etoile-deco-noel/", "capture", alt="Preview of the plan") }}

After making the cuts, simply glue the elements together with wood glue.

My cuts are not very clean; I still lack practice with the scroll saw, and especially the 5mm plywood I bought was of poor quality (the board was warped, and the outer surfaces were so thin that they chipped at the slightest cut, even with tape).

{{ responsive("/assets/etoile-deco-noel/", "scie-a-chantourner", alt="The star being cut on the scroll saw") }}
