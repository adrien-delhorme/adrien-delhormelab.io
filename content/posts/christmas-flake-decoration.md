Title: Christmas Decoration: 3D Wooden Snowflake
Summary: Plans to make 3D wooden snowflakes with a laser cutter
Date: 2024-12-23
Category: DIY
Tags: DIY, Wood, Crafts
Slug: christmas-flake-decoration
Id: christmas-flake-decoration
Lang: en

{% from "partials/img.html" import responsive %}
<div class="deux-colonnes">
    {{ responsive("/assets/flocon-deco-noel/", "flocon-deco-noel-bois-1", alt="3D wooden Christmas snowflake decoration") }}
    {{ responsive("/assets/flocon-deco-noel/", "flocon-deco-noel-bois-2", alt="3D wooden Christmas snowflake decoration") }}
</div>

During my visit to [8fablab](https://8fablab.fr/ "Voir le site du 8fablab"), I took advantage of the laser cutter to make some decorations for the Christmas tree (yes, [again...](christmas-star-decoration "Voir l'article sur l'étoile de Noël")).

There are many laser-cut snowflakes available online, but I wanted to make 3D snowflakes instead of flat ones.

So, I created my cutting file with two 2D snowflake models and the elements to create the branches of these snowflakes, rotated 90° relative to the others.
The shapes are very small, and after assembly with wood glue, the whole thing seems a bit fragile.

But the result is satisfying, and with a little paint to hide the laser-burned edges, the finish looks great.
<img src="/assets/flocon-deco-noel/flocons.svg" title="Shapes for cutting" /> [Download the SVG cutting file](/assets/christmas-flake-decoration/flocons.cut.svg)

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank"><img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" alt="CC BY-NC-SA 4.0" width="117" height="41" /></a>
