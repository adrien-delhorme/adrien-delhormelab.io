Title: Diecast car disassembling
Summary: Explanations and tools you will need to disassemble/reassemble a diecast car
Date: 2022-04-21
Category: DIY
Tags: DIY, Repair, Disassemble, Reassemble, Majorette, Toys, Diecast, Cars
Slug: diecast-cat-disassembling
Id: diecast-car-disassembling
Lang: en

{% from "partials/img.html" import responsive %}

When I was kid, I was a huge fan of diecast cars. These toys are pretty
resistant, but over time, painting and moving parts can be damaged (doors,
wheels axles). Even though the car cannot be disassembled easily (no screws,
but some kind of rivets), I always thougth it can be repaired. But I never
found good explanations to do this (which tools, which size for the screws,
etc), so here are my instructions to disassemble a diecast car and add a screw
to reassemble it.

The car here is a <a href="https://www.majorette.com" title="Go to Majorette website" target="_blank" rel="nofollow external">Majorette</a>,
but the instructions are the same for other brands.

### Tools to disassemble and reassemble a diecast car

- a large drill: Ø 4mm
- a tap holder, a M2×0.4mm tap and it’s corresponding Ø 1.6mm drill bit
- a M2×4mm screw and a screwdriver

Figuring by size of Majorette’s rivet stem width, I chose to use Ø 1.6 mm
screws. This size matches the <a href="https://armstrongmetalcrafts.com/Reference/MetricTapChart.aspx" target="_blank">M2 size for metric tapping</a>.

What I bought on a French model making tools store (<a href="https://micro-modele.fr/" target="_blank">micro-modele.fr</a>):

- <a href="https://micro-modele.fr/fr/tarauds-metriques-pas-a-droite-standard/6849-taraud-machine-din371-1-passe-hss-m2-1-piece.html" title="Tap on Micro model's website" target="_blank" rel="nofollow external">M2×0.4mm Tap (9.95€)</a>
- <a href="https://micro-modele.fr/fr/tarauds-metriques-pas-a-droite-standard/6899-porte-tarauds-pour-tarauds-de-m1-a-m8.html" title="Tap holder on Micro model's website" target="_blank" rel="nofollow external">Tap holder (5.95€)</a>
- <a href="https://micro-modele.fr/fr/388-din7985-m2" title="Screw on Micro model's website" target="_blank" rel="nofollow external">M2×4mm Screw (0.10€)</a>
- <a href="https://micro-modele.fr/fr/forets-a-metaux-hss-cobalt/6786-foret-hss-cobalt-10mm.html#/326-diametre-1_6_mm" title="Drill bit on Micro model's website" target="_blank" rel="nofollow external">Ø 1.6mm Drill bit (2.60€)</a>

{{ responsive("/assets/diecast-cat-disassembling/", "tools", alt="Tap holder,
tap, drill bit and screws" ) }}

### Steps to disassemble

#### Remove the car’s bottom

We first need to remove stem’s cap with a drill of same size (Ø 4mm in my case).

<img src="/assets/diecast-cat-disassembling/schema-stem.svg" style="width: 80%" />

<video controls loop>
  <source src="/assets/diecast-cat-disassembling/1-open.webm" type="video/webm">
</video>

{{ responsive("/assets/diecast-cat-disassembling/", "opened", alt="diecast car’s bottom separated from car") }}

At this point we are able to remove the car’s bottom to repaint the body of the
car, untwist the wheel axle or change the wheels.

Then we must put the car’s bottom in its place, and so replace the rivet by a
screw.

#### Drill the stem

We start to gently drill the stem to the desired depth (4mm long screws in my
case), with the Ø 1.6mm drill bit.

<video controls loop>
  <source src="/assets/diecast-cat-disassembling/2-drill.webm" type="video/webm">
</video>

{{ responsive("/assets/diecast-cat-disassembling/", "drilled", alt="drilled hole in stem" ) }}

#### Tap the hole

Then we start to tap the hole with the M2×0.4mm tap. Start with one turn, then
unscrew to let the chips come out. Repeat this until you reach the bottom.

<video controls loop>
  <source src="/assets/diecast-cat-disassembling/3-tap.webm" type="video/webm">
</video>

#### Reassemble and screw

Put back the car's bottom and place the screw in the hole. Tighten it gently.
Sometimes the screw is longer than the drill hole or the stem is higher than
the surface you want to fix: shorten it with a metal file or a <a href="https://www.dremel.com/gb/en/product-categories/multi-tools" title="Go to Dremel website" rel="nofollow external" target="_blank">Dremel tool</a>.

<video controls loop>
  <source src="/assets/diecast-cat-disassembling/4-screw.webm" type="video/webm">
</video>

{{ responsive("/assets/diecast-cat-disassembling/", "finished", alt="The finished diecast car") }}

The diecast car is now reassembled and easily disassembled if needed later.
