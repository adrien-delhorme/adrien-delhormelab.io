Title: Puzzle Roads
Summary: Roads to assemble for children to play with toy cars.
Date: 2023-04-01
Category: DIY
Tags: DIY, Majorette, Toy, Car, Road, Puzzle
Slug: puzzle-roads-v1
Id: puzzle-roads-v1
Lang: en

{% from "partials/img.html" import responsive %}

I have always loved playing with toy cars as a child. My favorite thing was to roll them on the roads of my play mat or build cities with other toys to drive the cars inside.

I remember being amazed when I had the chance to play with [majo-kit](https://en.m.wikipedia.org/wiki/Majo-kit): kits of plastic parts (sidewalks, crosswalks, shelters, streetlights) to build a miniature city! It was incredible and so realistic!
{{ responsive("/assets/routes-puzzle-v1/", "majo-kit", alt="Majokit") }}

These majo-kits were a great commercial success for Majorette in the 80s. Some competitors also developed their own ranges of miniature cities to build (like Matchbox Motorcity, for example).
{{ responsive("/assets/routes-puzzle-v1/", "motor-city", alt="Motor city 300") }}

Today, there doesn't seem to be a modern equivalent to these modular city-building systems.

However, there are several road assembly games: my favorite is [Waytoplay](https://www.waytoplay.toys/) with their flexible plastic roads. There were also the wooden roads from [Brio](https://www.brio.fr/) (known for their wooden trains and tracks), but they no longer seem to be for sale on their website. Finally, there are felt road assembly games (like those from [Hema](https://www.hema.com/fr-fr/enfant/jouets/puzzles/puzzle-route-feutrine---27-pieces-15100061.html), for example), which often remain very basic in design.

{{ responsive("/assets/routes-puzzle-v1/", "hema-routes-puzzle", alt="Hema road puzzle") }}

What I find most frustrating about these different road games is that they are too simplistic and do not allow for the creation of complex circuits like Brio tracks.

Since my children are also little fans of miniature cars, I started imagining the road game I would have dreamed of at their age.

## Specifications

- Road tiles to assemble to create a circuit or network
- Road width suitable for 1/64 scale miniature vehicles
- True to reality and its diversity (i.e., not based on a single model of roads, signage, etc.)
- Allowing complex assemblies (multiple intersections, turns with different angles, etc.)
- Constructible with common materials and open-source software
- Durable, bio-sourced, and as recyclable as possible

## Realization

### Hexagonal System
I opted for a hexagonal tile system because it allows for error-free tiling: as long as the tiles touch, they fit together without any possible errors. This is not always the case with other systems, which can sometimes have configurations where the pieces do not join: a bit frustrating for children.

With a radius of 100 mm, the roads are about the same width as Waytoplay roads.

### CAD and Printing
A bit of [Inkscape](https://inkscape.org/) to create the hexagons and road traces. To have an interesting number of pieces (30), I chose the A0 format.

After exporting to PDF, I went to a printer to put these roads on standard white paper.

### Backing and Cutting
I used a 3 mm thick cardboard to ensure some rigidity to the tiles. I would have preferred a gray cardboard made from recycled paper, but I couldn't find any near me.

The dimensions of the cardboards I found in stores (used for framing) are not to international standards but to French standards. The closest format to A0 (1,189 × 841 mm) is the "Grand Monde" format (1,260 × 900 mm). That will do.

Using a spray adhesive purchased from a craft store, I glued my printed sheet onto the cardboard backing. Finally, I patiently cut out the tiles with a cutter.

{{ responsive("/assets/routes-puzzle-v1/", "planche-avant-découpe", alt="The road board before cutting") }}

## The Result

{{ responsive("/assets/routes-puzzle-v1/", "routes-puzzle-v1", alt="Puzzle roads v1 once completed") }}
The result is cool, but the first feedback from demanding users is harsh: it's lame, the roads don't stay together!

Indeed, I knew the tiles would move while playing, but it was impossible to make a cut with tabs (like puzzle pieces) with a cutter, on such a large number of pieces and such thick material.

This version already allows my children and me to experiment with the construction possibilities. It is possible to make intersections, roundabouts, a highway entrance/exit, etc., but we lack some straight lines (there are too many intersections), and the two tight turns are way too tight.

[I'll do better in v2!](/puzzle-roads-v2)
