Title: Routes puzzle
Summary: Des routes à assembler pour que les enfants jouent aux petites voitures.
Date: 2023-04-01
Category: DIY
Tags: DIY, Majorette, Jouet, Voiture, Route, Puzzle
Slug: routes-puzzle-v1
Id: puzzle-roads-v1
Lang: fr

{% from "partials/img.html" import responsive %}

Jʼai toujours aimé jouer aux petites voitures étant petit. Ce que je préférais était de les faire rouler sur les routes de mon tapis de sol ou construire des villes avec dʼautres jouets, pour faire circuler les voitures à lʼintérieur.

Je me rappelle avoir été émerveillé lorsque jʼai eu lʼoccasion de jouer avec des [majo-kit](https://en.m.wikipedia.org/wiki/Majo-kit) : des kits de pièces en plastique (trottoirs, passages piétons, abris, lampadaires) permettant de construire une ville miniature ! Cʼétait incroyable et tellement réaliste !
{{ responsive("/assets/routes-puzzle-v1/", "majo-kit", alt="Majokit") }}

Ces majo-kits ont été un grand succès commercial pour Majorette dans les années 80. Certains concurrents ont aussi développés leurs propres gammes de villes miniatures à construire (Matchbox Motorcity par exemple).
{{ responsive("/assets/routes-puzzle-v1/", "motor-city", alt="Motor city 300") }}

Aujourdʼhui il ne semble pas exister dʼéquivalent moderne à ces systèmes modulaires de construction de villes.

On trouve par contre plusieurs jeux de routes à assembler : celle que je préfère est [Waytoplay](https://www.waytoplay.toys/) avec leurs routes en plastique souple. Il existait aussi les routes en bois de [Brio](https://www.brio.fr/) (connue pour ses trains et rails en bois) mais elles ne semblent plus en vente sur leur site internet. Enfin on trouve des jeux de routes à assembler en feutrine (comme celles dʼ[Hema](https://www.hema.com/fr-fr/enfant/jouets/puzzles/puzzle-route-feutrine---27-pieces-15100061.html) par exemple), qui restent souvent très sommaire dans leur conception.

{{ responsive("/assets/routes-puzzle-v1/", "hema-routes-puzzle", alt="Routes puzzle Hema") }}

Ce que je trouve le plus frustrant dans ces différents jeux de routes, cʼest quʼils sont trop simplistes et ne permettent pas de créer des circuits complexes à la manière des rails Brio.

Mes enfants étant également de petits amateurs de voitures miniatures, jʼai commencé à imaginer le jeu de routes dont jʼaurai rêvé à leur âge.

## Le cahier des charges

- tuiles de routes à assembler pour créer un circuit ou un réseau
- largeur de route adaptées à des véhicules miniature échelle 1/64<sup>e</sup>
- fidèle à la réalité et à sa diversité (cʼest-à-dire qui nʼest pas calqué sur un modèle unique de routes, signalétiques, etc)
- permettant des assemblages complexes (intersections multiples, virages avec des angles différents, etc)
- constructible avec des matériaux communs et des logiciels open-source
- durable, biosourcé, recyclable au maximum

## La réalisation

### Système hexagonal 
Jʼai opté pour un système de tuiles hexagonal car il permet un tuilage sans erreurs : à partir du moment où les tuiles se touchent, elles se disposent côte à côte sans erreurs possible. Ça nʼest pas toujours le cas des autres systèmes qui peuvent parfois des configuration où les pièces ne se joignent pas : un peu frustrant pour les enfants.

Dʼun rayon de 100 mm les routes font a peu près la même largeur que les routes Waytoplay.

### DAO et impression
Un peu de [Inkscape](https://inkscape.org/) pour créer les hexagones et les tracés des routes. Pour avoir un nombre intéressant de pièces (30), je choisi le format A0.

Après un export en PDF je me suis rendue chez un imprimeur pour poser ces routes sur du papier blanc standard.

### Contre-collage support et découpe
Jʼai utilisé un carton bois de 3 mm dʼépaisseur pour assurer une certaine rigidité aux tuiles. Jʼaurai préféré un carton gris à base de papier recyclé, mais je nʼen ai pas trouvé près de chez moi.

Les dimensions des cartons que jʼai trouvé dans le commerce (utilisés pour lʼencadrement) ne sont pas à la norme internationale, mais à la norme française. Le format le plus proche du A0 (1 189 × 841 mm) est le format "Grand Monde" (1260 × 900 mm). Ça fera lʼaffaire.

Grâce à une colle en spray achetée dans un magasin de loisirs créatifs, jʼai contre-collé ma feuille imprimée sur mon support carton. Jʼai enfin patiemment découpé les tuile avec un cutter.

{{ responsive("/assets/routes-puzzle-v1/", "planche-avant-découpe", alt="La plance de routes avant la découpe") }}

## Le résultat

{{ responsive("/assets/routes-puzzle-v1/", "routes-puzzle-v1", alt="Routes puzzle v1 une fois terminées") }}
Le rendu est cool, mais les premiers retours de utilisateurs intransigeants sont cinglants : cʼest nul les routes ne tiennent pas entre elles !

Effectivement je savais bien que les tuiles se déplaceraient en jouant, mais impossible de faire une découpe avec des ergots (comme les pièces de puzzle) avec un cutter, sur un si  grand nombre de pièces et un matériaux si épais.

Cette version nous permet déjà, les enfants et moi, dʼexpérimenter les possibilités de construction. Il est possible de faire des croisements, des ronds points, une entrée/sortie dʼautoroute, etc, mais il nous manque un peu de lignes droites (il y a trop dʼintersections) et les deux virages serrés sont beaucoup trop serrés.

[Je ferai mieux dans la v2 !](/routes-puzzle-v2)
