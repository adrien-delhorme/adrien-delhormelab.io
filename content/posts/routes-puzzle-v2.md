Title: Routes puzzle v2
Summary: La version améliorée des routes à assembler pour que les enfants jouent aux petites voitures.
Date: 2024-05-03
Category: DIY
Tags: DIY, Majorette, Jouet, Voiture, Route, Puzzle
Slug: routes-puzzle-v2
Id: puzzle-roads-v2
Lang: fr

{% from "partials/img.html" import responsive %}

[Les premiers résultats étant concluants](/fr/routes-puzzle-v1.html), je me lance dans une version améliorée de mes routes.

## Le nouveau cahier des charges

- tuiles de routes à assembler pour créer un circuit ou un réseau
- 🆕 les éléments tiennent ensemble grâce à des ergots hermaphrodites (mâle et femelle en même temps) ce qui nʼimpose pas de sens à la tuile
- largeur de route adaptées à des véhicules miniature échelle 1/64
- fidèle à la réalité et à sa diversité
- permettant des assemblages complexes (intersections, virages, etc)
- constructible avec des matériaux <del>communs</del> accessibles et des logiciels open-source
- durable, biosourcé, recyclable au maximum
- 🆕 la découpe des éléments ne suit pas la forme hexagonale, mais suit la forme de la route

## La réalisation

Les étapes sont (presque) les mêmes que pour [la v1](/fr/routes-puzzle-v1.html). Les différences sont les suivantes :

### Ergots hermaphrodites

<img src="/assets/routes-puzzle-v2/types-ergots.svg" />

Pour que les tuiles tiennent entre elles, il me faut des ergots comme les pièces de puzzle par exemple. Mais il y a deux inconvénients majeurs à ce système mâle/femme des pièces de puzzle :

1. On ne peut pas pivoter une tuile pour sʼen servir dans lʼautre sens : mâle/mâle ou femelle/femelle ça ne fonctionne pas.
2. Si on introduit des tuiles &laquo;&nbsp;embranchements&nbsp;&raquo; on va probablement se retrouver à vouloir connecter deux mâles ou deux femelles : un adaptateur mâle/mâle ou femelle/femelle sera nécessaire.

Par contre, un avantage est que lʼon peut retourner (recto/verso) les tuiles pour changer le sens dʼun virage pas exemple, mais dans ce cas, elle doit être imprimée des deux côtés : cʼest la solution choisie par Waytoplay, Brio et presque la totalité des autres concurrents.

Dʼexpérience avec les rails Brio, les adaptateurs M/M ou F/F sont une contrainte, et je nʼai pas lʼintention dʼimprimer mon support des deux côtés : je choisi donc naturellement des ergots hermaphrodites, cʼest à dire qui sont mâle et femelle en même temps.

### Découpe laser

Habitant non loin dʼun très bon [fablab](https://8fablab.fr), je vais passer mon permis sur la découpeuse/graveuse Laser System 1490. Cela rend réalisable les découpes des ergots et jʼen profite aussi pour découper les tuiles à la forme des routes imprimées plutôt que de garder les &laquo;&nbsp;blancs&raquo;&nbsp;.

{{ responsive("/assets/routes-puzzle-v2/", "decoupeuse-laser", alt="Les routes en train dʼêtre découpées dans la Laser System 1490 du fablab") }}

### Amélioration des tracés des routes

Jʼen profite pour améliorer les tuiles de virages serrés et de parking. Jʼajoute des tuiles &laquo;&nbsp;chicanes&nbsp;&raquo;, et jʼabandonne la tuile &laquo;&nbsp;pont&nbsp;&raquo;. Les fichiers sources pour lʼimpression et la découpe sont libres et téléchargeables juste ici :

<div class="deux-colonnes">
    <ul>
        <li><a href="/assets/routes-puzzle-v2/impression.pdf" title="Télécharger impression.pdf">impression.pdf</a></li>
        <li><a href="/assets/routes-puzzle-v2/impression.source.svg" title="Télécharger impression.source.svg">impression.source.svg</a></li>
        <li><a href="/assets/routes-puzzle-v2/découpe.svg" title="Télécharger découpe.svg">découpe.svg</a></li>
    </ul>
    <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank"><img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" alt="CC BY-NC-SA 4.0" width="117" height="41" /></a>
</div>

## Le résultat

Le rendu est encore mieux. Les tuiles tiennent parfaitement ensemble mais restent très faciles à assembler/séparer. La découpe au plus prêt du tracé donne un aspect Waytoplay.

{{ responsive("/assets/routes-puzzle-v2/", "routes-puzzle-v2", alt="Routes puzzle v2 terminées") }}

Mais il y a (forcément) de nouveaux problèmes :

- le calibrage de la découpeuse laser a été laborieux, et la découpe sʼest donc décalée du tracé de lʼimpression aux extrémités,
- les nombreuses formes "saillantes" des ergots favorisent le décollement de la feuille imprimée : la colle en spray nʼest pas assez forte,
- la découpe laser induit une forte odeur de bois brûlé, pas très agréable, même si elle sʼestompe avec le temps.

Peut-être que jʼaméliorerai ces points dans le v3 !
