Title: Décoration de Noël : étoile en bois
Summary: Les plans pour découper une étoile en bois à la scie à chantourner
Date: 2023-01-03
Category: DIY
Tags: DIY, Bois, Bricolage
Slug: etoile-deco-noel
Id: christmas-star-decoration
Lang: fr

{% from "partials/img.html" import responsive %}

{{ responsive("/assets/etoile-deco-noel/", "etoile-deco-noel-bois", alt="Décoration étoile de Noël en bois") }}

Chaque fin d’année, nous avons pris l’habitude de fabriquer des nouvelles décorations pour le sapin de Noël. Je suis donc parti à la recherche sur l’interweb, d’inspiration pour fabriquer quelque chose de joli, de préférence en bois, histoire de faire un peu tourner ma scie à chantourner.

Au détour d’une recherche par image, je suis tombé sur une photo d’étoile qui m’a fortement tappée dans l’œil, et j’ai donc décidé de la reproduire.

Je partage ici les plans SVG que j’ai dessiné avec <a href="https://inkscape.org/fr/" title="Se rendre sur le site d’Inkscape">Inkscape</a>, et qui est facilement adaptable (formes, dimensions)&nbsp;:

<ul>
  <li><a href="/assets/etoile-deco-noel/plan-etoile-bois.pdf" title="Télécharger le plan PDF de l’étoile en bois">Plan PDF de l’étoile en bois</a></li>
  <li><a href="/assets/etoile-deco-noel/source-plan-etoile-bois.svg" title="Télécharger les sources du plan SVG de l’étoile en bois">Sources du plan SVG de l’étoile en bois</a></li>
</ul>

{{ responsive("/assets/etoile-deco-noel/", "capture", alt="Aperçu du plan") }}

Après avoir fait les découpes, il faut simplement coller les éléments entre eux avec de la colle à bois.

Mes découpes ne sont vraiment pas propres, je manque encore de pratique sur la scie à chantourner, et surtout le contreplaqué 5mm que j’ai acheté était d’une qualité pitoyable (le panneau était voilé et les surfaces extérieures tellement fines qu’elles éclates à la moindre découpe, même avec du scotch).

{{ responsive("/assets/etoile-deco-noel/", "scie-a-chantourner", alt="L'étoile en cours de découpe sur la scie à chantourner") }}
