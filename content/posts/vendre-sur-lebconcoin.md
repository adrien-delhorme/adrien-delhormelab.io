Title: Bien vendre sur Leboncoin
Summary: Quelques techniques pour que vos annonces se vendent bien et vite sur Leboncoin.
Date: 2023-04-23
Category: Réfléxions
Tags: 
Slug: vendre-sur-leboncoin
Id: vendre-sur-leboncoin
Lang: fr

# Comment bien vendre sur Leboncoin

Depuis plusieurs années, je m'efforce avant d'acheter un produit neuf, de toujours vérifier s'il n'est disponible sur un site de vente entre particuliers.

Dans la très grande majorité des cas, je trouve ce que je cherche. Mais ça me demande souvent de la patience et de la persévérence pour arriver à conclure un achat : la qualité des annonces fait perdre beaucoup de temps.

Vinted a un certain nombre de fonctionnalités qui aide les vendeurs et vendeuses à créer des annonces de bonne qualité. Leboncoin, lui est beaucoup plus laxiste et permet donc de créer des annonces de très mauvaise qualité.

Les annonces de mauvaise qualité ne se vendent pas (à l'exception de certains produits rares et recherchés) ou se vendent à un prix bien inférieur à leur valeur.

Après quelques années à écumer les sites de vente entre particuliers, je décris ici les points qui, selon moi, font qu'une annonce est de bonne qualité.

## L'annonce possède de nombreuses photos

Un trop grand nombre d'annonces manques de photos. Les photos permettent de rassurer les acheteurs :
- sur le fait que le produit est bien celui qu'ils recherchent
- sur l'état du produit réel
- sur la présence ou non de certains accessoirs

Lorsque c'est possible, il faut faire des photos du produit dans ses différents états d'utilisation (ouvert, fermé, allumé, éteint, démonté, remonté, dans et hors de son emballage...).

Des photos d'ensemble avec tous les accessoires, mais aussi des photos rapprochées sur des détails importants.

Il est parfois utile de mettre la photo du produit d'origine, que vous trouverez sur le site du fabricant : ça permet à l'acheteur de confirmer que c'est bien le même produit que le neuf. Mais il ne faut pas que ce soit la seule photo : il est indispensable d'avoir une photo du produit réel : il est trop risqué pour un acheteur de commander un produit sans en avoir vu une seule photo !

## L'annonce donne les informations précises

Pour vendre un article il faut :
- que les acheteurs trouvent l'annonce lors d'une recherche,
- qu'ils soient assurés que c'est le bon modèle, la bonne version
- qu'ils soient assurés que rien ne manque, que tous les accessoires sont présents.

Le titre et la description de l'annonce devront au moins contenir le nom de la marque et le nom du modèle du produit.

La description, elle devra mentionner toutes les caractéristiques du produit. Si l'article est encore en vente sur le site du fabricant, il ne faut pas hésiter à copier/coller le contenu de la fiche produit, dans votre annonce. Pour les objets volumineux, précisez les dimensions, le poid, si ça rentre dans une voiture ou une camionette, si certaines parties se démontent pour le transport, etc.

Un descriptif complet permet aussi d'avoir beaucoup de mots clés qui feront remonter votre article dans les recherches des acheteurs.

## L'annonce doit mentionner les éventuels défauts de l'article

Si vous ne voulez pas que votre article soit refusé par l'acheteur (vous ne serez pas payé), et que celui-ci écrive un commentaire désagréable sur votre profile, il vaut mieux éviter les mauvaises surprises.

Indiquez clairement dans la description de votre annonce et montrez sur les photos, les éventuels défauts de votre article.

Les défauts seront probablement une occasion pour l'acheteur de négocier le prix, mais en vendant en toute transparence, vous éviterez les litiges.

## Le vendeur répond rapidement

Cela paraît évident mais il est bon de le rappeler : s'il y a beaucoup d'annonces similaires à la votre, vous serez en concurrence avec les autres vendeurs. Le premier qui répondra aux sollicitations des acheteurs sera probablement le premier qui vendra.

## Le vendeur propose la livraison

Si l'article est éligible à la livraison par transporteur, proposez là, vous toucherez une bien plus grande quantité d'acheteurs potentiels.

Pour les articles trop lourd et/ou volumineux et donc non éligibles à la livraison, si vous voulez vendre au meilleur prix, quitte à faire un peu plus d'efforts, vous pouvez indiquer dans votre annonce que vous acceptez de "livrer" le produit à certains endroits. Par exemple à un point de rendez-vous situé entre votre domicile et votre travail, ou un lieu où vous vous rendez le weekend. Cela permettra de toucher plus d'acheteurs potentiels et donc de vendre plus rapidement et à un meilleur prix.

## L'annonce donne un prix

Il y a une mode parmi les vendeurs, qui consiste à écrire dans son annonce : "faire offre raisonnable".

C'est un petit jeu de devinette qui consiste pour l'acheteur à deviner le prix auquel le vendeur acceptera de céder son article...

C'est une perte de temps pour tout le monde. Si vous souhaitez vendre votre article, indiquez un prix sur votre annonce.

## Le prix de l'annonce est moins élevé que celui du produit neuf

Je ne m'intéresse pas ici aux articles qui sont rares et qui ont donc un prix qui n'est plus corrélé à leur valeur initiale.

La plus grosse erreur qui empêche un article d'être vendu, est d'après moi de mettre un prix inadapté.

Il n'est pas rare de voir des articles d'occasion (mais "jamais servi" ou "encore emballés") vendus au même prix que le produit neuf en magasin (ou un prix très légèrement inférieur).

Je pense que certaines personne n'ont pas conscience qu'un produit neuf (même encore emballé) acheté auprès d'un particulier, n'a plus la même valeur.

Il n'a par exemple plus la même période de garantie : la garantie débute immédiatement après l'achat en magasin. Et cet achat doit être prouvé avec une facture (il faudrait, de plus, veiller à récupérer cette facture auprès du vendeur sur Leboncoin).

Il y a aussi une plus grande incertitude sur l'état du produit : est-ce qu'il a été stocké dans de bonnes conditions ? Est-ce qu'il a été correctement emballé et protégé pour son expédition (en cas de livraison) ? Les vendeurs particuliers sur Leboncoin ne sont pas des professionnels, et il arrive parfois que les emballages ne soient pas du tout adaptés aux articles expédiés (et donc les articles arrivent endommagés). Enfin, l'achat auprès d'un particulier est beaucoup plus chronophage qu'un achat dans une boutique physique ou en ligne : il faut souvent chercher longtemps avant de trouver le bon produit (l'interface du site et des applis Leboncoin n'aidant pas...), il faut régulièrement contacter les vendeurs pour demander des précisions (car les annonces sont imprécises), et parfois attendre plusieurs jours avant qu'il ne réponde ! Toutes ces abscences de commodités ont un prix, que le vendeur doit prendre en compte lorsqu'il détermine son prix de vente.

## L'annonce a un prix cohérent avec l'offre

Beaucoup (beaucoup...) de vendeurs amateurs ne sont pas conscient que l'article qu'ils vendent a perdu (et perdra encore) de la valeur avec le temps. Certains ne voient donc pas le problème à essayer de vendre un produit d'occasion, plus cher que ce que proposent certaines boutiques en ligne qui vendent encore le produit neuf...

Vous ne serez pas totalement libre dans la détermination du prix : s'il y a des centaines de produits similaires au votre en vente, alignez vous avec le moins cher (qui est dans le même état). Sinon les acheteurs acheteront les articles des autres  vendeurs ! Quand il y a vraiment beaucoup d'articles similaires en vente, cela veut dire qu'il y a plus d'offre que de demande et donc vous ne vendrez pas sans baisser le prix.

Si vous avez créé une anonce de bonne qualité en suivant les conseils précédents, mais que vous n'arrivez pas à vendre votre article, c'est que personne n'en veut à ce prix là. Baissez le prix jusqu'à trouver preneur.

Que vous gardiez un article dans vos placards ou que vous essayiez de le vendre sur Leboncoin : plus le temps passe et plus votre article perd de la valeur. Vendez rapidement !


## Et si l'article ne se vend toujours pas ?

Si vous avez suivi les conseils de cet article, vous devriez avoir baissé le prix jusqu'à trouver preneur. Le prix peut donc atteindre 0€ ! Si personne ne souhaite prendre le produit que vous donnez gratuitement, vous pouvez toujours essayer d'autre sites et applications spécialisés dans le don. Si ça ne fonctionne pas, il est temps de l'apporter à la déchetterie.
