Title: Spécifications du sytème de tuiles
Summary: Les spécifications du système qui permet de créer des tuiles de routes
Date: 2025-02-09
Category: DIY
Tags: DIY, Majorette, Jouet, Voiture, Route, Puzzle, Spécification, Système
Slug: tuiles-specs
Id: tiles-specs
Lang: fr

{% from "partials/img.html" import responsive %}

# <a name="specifications-systeme-de-tuiles" href="#specifications-systeme-de-tuiles">Spécifications système de tuiles</a>

Ces spécifications sont les fondations dʼun sytème de tuiles à assembler. En nommant et en définissant les différents concepts, il devient plus simple de concevoir des tuiles cohérentes et qui sʼinterfacent entre elles.

## <a name="tuile" href="#tuile">Tuile</a>
La tuile est lʼélément de base. Lʼassemblage de tuiles permet de créer un plateau.
Les tuiles peuvent avoir différentes formes : triangle, carré, polygones. Lorsque les tuiles seront assemblées côte à côte, les interfaces de deux tuiles adjacentes devront avoir la même taille.

<img src="/assets/tiles-specs/formes.svg" />

## <a name="interface" href="#interface">Interface</a>
Les interfaces sont les arrêtes des tuiles. Cʼest par leurs intefaces que les tuiles sont assemblées.

<img src="/assets/tiles-specs/interfaces.svg" />

### <a name="taille-de-linterface" href="#taille-de-linterface">Taille de lʼinterface</a>
La taille dʼune interface est sa longueur. Les interfaces de même taille peuvent éventuellement être assemblées (à condition que les connecteurs soient compatibles).

<img src="/assets/tiles-specs/interfaces-tailles.svg" />

### <a name="connecteurs" href="#connecteurs">Connecteurs</a>
Les connecteurs sont les éventuels ergots situés sur les interfaces, qui permettent de maintenir les tuiles entre elles et dʼimposer un sens dʼassemblage aux tuiles.

Une tuile qui présente au moins deux types de connecteurs différents est appelée un [adaptateur de connecteur](#adaptateur-de-connecteur).

<img src="/assets/tiles-specs/connecteurs.svg" />

### <a name="acces" href="#acces">Accès</a>
Les accès sont les points dʼentrées/sorties dʼune tuile, situés aux interfaces. Ils peuvent être reliés ou non, à dʼautres accès de la même tuile, par des voies. Il peut y avoir plusieurs accès sur une interface ou aucun.

<img src="/assets/tiles-specs/accès-et-voies.svg" />

#### <a name="largeur-dacces" href="#largeur-dacces">Largeur dʼaccès</a>
Cʼest la largeur de la voie au niveau de lʼaccès. Les largeurs des accès doivent être identiques de chaque côté de lʼinterface.

#### <a name="type-dacces" href="#type-dacces">Type dʼaccès</a>
Cʼest le type de voie au niveau de lʼaccès (par exemple : route, chemin, fleuve, piste, rails, ...). Le type de lʼaccès doit être le même de chaque côté de lʼinterface.

### <a name="voie" href="#voie">Voie</a>
Une voie est ce qui relie deux accès dʼune même tuile.

Une voie peut relier des accès de même type ou des accès de types différents. Une tuile qui possède au moins une voie reliant des accès de types différents est appelée un [adaptateur de voies](#adaptateur-de-voies).

<img src="/assets/tiles-specs/voies.svg" />

#### <a name="sens-de-circulation-dune-voie" href="#sens-de-circulation-dune-voie">Sens de circulation dʼune voie</a>
Il peut être : bidirectionnel ou à sens unique (dʼun accès vers un ou plusieurs autres).

## <a name="style-dhabillage" href="#style-dhabillage">Style dʼhabillage</a>
Cʼest lʼaspect graphique de la tuile.

Une tuile qui possède au moins deux styles dʼhabillage différents à ses interfaces est appelée [adaptateur de style](#adaptateur-de-style).

<div class="quatre-colonnes">
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-illustration", alt="Style illustration") }}
        <figcaption>Style illustration</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-vecteurs", alt="Style vecteurs") }}
        <figcaption>Style vecteur</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-peinture", alt="Style peinture") }}
        <figcaption>Style peinture</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-croquis", alt="Style croquis") }}
        <figcaption>Style croquis</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-france.svg" alt="Style France">
        <figcaption>Style France</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-irelande.svg" alt="Style Irelande">
        <figcaption>Style Irelande</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-norvège.svg" alt="Style Norvège">
        <figcaption>Style Norvège</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-circuit.svg" alt="Style circuit">
        <figcaption>Style circuit</figcaption>
    </figure>
</div>

### <a name="nom-du-style" href="#nom-du-style">Nom du style</a>
Cʼest le nom du style dʼhabillage.

### <a name="tags-du-style" href="#tags-du-style">Tags du style</a>
Ce sont des mots clés décrivant le style. Par exemple "photoréalisme", "aquarelles", "pixelart" pour le style de dessins.

"France", "Japon", "Suède" pour le style de signalisation horizontale.

"nuit", "pluie", "neige", "campagne", "ville" pour le style de lʼenvironnement.

## <a name="adaptateur" href="#adaptateur">Adaptateur</a>
### <a name="adaptateur-de-connecteur" href="#adaptateur-de-connecteur">Adaptateur de connecteur</a>
Cʼest une tuile qui permet de passer dʼun type de connecteur à un autre.

### <a name="adaptateur-de-voies" href="#adaptateur-de-voies">Adaptateur de voies</a>
Cʼest une tuile dont au moins une voie possède des accès de types différents.

### <a name="adaptateur-de-style" href="#adaptateur-de-style">Adaptateur de style</a>
Cʼest une tuile qui permet de passer dʼun style dʼhabillage à un autre.
