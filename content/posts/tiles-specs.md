Title: Tile System Specifications
Summary: Specifications for the system that allows creating road tiles
Date: 2025-02-09
Category: DIY
Tags: DIY, Majorette, Toy, Car, Road, Puzzle, Specification, System
Slug: tile-specs
Id: tiles-specs
Lang: en

{% from "partials/img.html" import responsive %}

# <a href="#tile-system" name="tile-system">Tile System Specifications</a>

These specifications are the foundations of a tile assembly system. By naming and defining the different concepts, it becomes easier to design coherent tiles that interface with each other.

## <a href="#tile" name="tile">Tile</a>
The tile is the basic element. The assembly of tiles allows the creation of a board.
Tiles can have different shapes: triangle, square, polygons. When tiles are assembled side by side, the interfaces of two adjacent tiles must be the same size.

<img src="/assets/tiles-specs/formes.svg" />

## <a href="#interface" name="interface">Interface</a>
Interfaces are the edges of the tiles. It is through their interfaces that the tiles are assembled.

<img src="/assets/tiles-specs/interfaces.svg" />

### <a href="#interface-size" name="interface-size">Interface Size</a>
The size of an interface is its length. Interfaces of the same size can potentially be assembled (provided the connectors are compatible).

<img src="/assets/tiles-specs/interfaces-tailles.svg" />

### <a href="#connectors" name="connectors">Connectors</a>
Connectors are the possible tabs located on the interfaces, which allow the tiles to be held together and impose an assembly direction on the tiles.

A tile that has at least two different types of connectors is called a [connector adapter](#connector-adapter).

<img src="/assets/tiles-specs/connecteurs.svg" />

### <a href="#access-points" name="access-points">Access Points</a>
Access points are the entry/exit points of a tile, located at the interfaces. They can be connected or not to other access points of the same tile, by paths. There can be multiple access points on an interface or none.

<img src="/assets/tiles-specs/accès-et-voies.svg" />

#### <a href="#access-width" name="access-width">Access Width</a>
This is the width of the path at the access point. The widths of the access points must be identical on each side of the interface.

#### <a href="#access-type" name="access-type">Access Type</a>
This is the type of path at the access point (e.g., road, path, river, track, rails, etc.). The type of access must be the same on each side of the interface.

### <a href="#path" name="path">Path</a>
A path is what connects two access points of the same tile.

A path can connect access points of the same type or access points of different types. A tile that has at least one path connecting access points of different types is called a [path adapter](#path-adapter).

<img src="/assets/tiles-specs/voies.svg" />

#### <a href="#" name="">Path Direction</a>
It can be: bidirectional or one-way (from one access point to one or more others).

## <a href="#style" name="style">Style</a>
This is the graphic appearance of the tile.

A tile that has at least two different styles at its interfaces is called a [style adapter](#style-adapter).

<div class="quatre-colonnes">
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-illustration", alt="Illustration Style") }}
        <figcaption>Illustration Style</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-vecteurs", alt="Vector Style") }}
        <figcaption>Vector Style</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-peinture", alt="Painting Style") }}
        <figcaption>Painting Style</figcaption>
    </figure>
    <figure>
        {{ responsive("/assets/tiles-specs/", "style-croquis", alt="Sketch Style") }}
        <figcaption>Sketch Style</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-france.svg" alt="France Style">
        <figcaption>France Style</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-irelande.svg" alt="Ireland Style">
        <figcaption>Ireland Style</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-norvège.svg" alt="Norway Style">
        <figcaption>Norway Style</figcaption>
    </figure>
    <figure>
        <img src="/assets/tiles-specs/style-circuit.svg" alt="Circuit Style">
        <figcaption>Circuit Style</figcaption>
    </figure>
</div>

### <a href="#style-name" name="style-name">Style Name</a>
This is the name of the style.

### <a href="#style-tags" name="style-tags">Style Tags</a>
These are keywords describing the style. For example, "photorealism," "watercolors," "pixel art" for drawing styles.

"France," "Japan," "Sweden" for horizontal signage styles.

"Night," "rain," "snow," "countryside," "city" for environmental styles.

## <a href="#adapter" name="adapter">Adapter</a>
### <a href="#connector-adapter" name="connector-adapter">Connector Adapter</a>
This is a tile that allows switching from one type of connector to another.

### <a href="#path-adapter" name="path-adapter">Path Adapter</a>
This is a tile where at least one path has access points of different types.

### <a href="#style-adapter" name="style-adapter">Style Adapter</a>
This is a tile that allows switching from one style to another.
