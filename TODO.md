# Todo
- [ ] Nouveaux articles
- [ ] Optimiser la taille des fonts pour le code
- [ ] Traduire les "readmore" les dates et "about" en fr
- [ ] Refaire l'icône du header et la favicon
- [ ] pas de H1 dans les articles

# Done
- [X] Thème sombre
- [x] Icône lien externe moche
- [x] Sélecteur de langue en homepage
- [x] gestion des images responsives (avec script pour redimensionner aux X tailles nécessaires)
  - max-width 650px (1300px @2x, 2600px @4x)
	- min-width 320px (640px @2x, 2560px @4x)
	- tailles [350, 650, 1300, 2600]
	- template d'include pour les images
	- https://pypi.org/project/pelican-image-process/
