#!/usr/bin/env python

AUTHOR = 'Adrien Delhorme'
SITENAME = 'Delhor.me'
SITEURL = ''
SITE_META_TITLE = "Delhor.me - Blog"
SITE_META_DESCRIPTION = "Projects and thoughts I finally decided to share with the Interweb"

PATH = 'content'
OUTPUT_PATH = 'public'
STATIC_PATHS = [
    'favicon',
    'assets',
]
EXTRA_PATH_METADATA = {
    'favicon/apple-touch-icon.png': {'path': 'apple-touch-icon.png'},
    'favicon/favicon-96x96.png': {'path': 'favicon-96x96.png'},
    'favicon/favicon.ico': {'path': 'favicon.ico'},
    'favicon/favicon.svg': {'path': 'favicon.svg'},
    'favicon/site.webmanifest': {'path': 'site.webmanifest'},
    'favicon/web-app-webmanifest-192x192.png': {'path': 'favicon/web-app-webmanifest-192x192.png'},
    'favicon/web-app-webmanifest-512x512.png': {'path': 'favicon/web-app-webmanifest-512x512.png'},
}

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%b %Y'
DATE_FORMATS = {  # locale -a
    'en': ('en_US.utf-8', '%b %Y'),
    'fr': ('fr_FR.utf-8', '%b %Y')
}


DEFAULT_LANG = 'en'
ARTICLE_TRANSLATION_ID = 'id'

# Menu
DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "theme"

# Plugins
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['pelican.plugins.i18n_subsites', 'jinja2content']

I18N_SUBSITES = {
    'fr': {
        'SITENAME': 'Delhor.me',
    },
}


def get_language_name(lang_code):
    languages_lookup = {
         'fr': 'Français',
         'en': 'English',
     }
    return languages_lookup[lang_code]

JINJA_FILTERS = {
     'get_language_name': get_language_name,
 }
