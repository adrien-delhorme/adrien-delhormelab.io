import os
import sys
from PIL import Image

DEBUG = False
FOLDER = sys.argv[1]
IMG_EXTENSIONS = [".jpg", ".jpeg", ".png"]
IMG_WIDTHS = [350, 650, 1300, 2600]

str_extensions = ", ".join(IMG_EXTENSIONS)
print(f"\nConvert to Webp and create resonsive images for all {str_extensions} files in {FOLDER}...")
for root, dirs, files in os.walk(FOLDER):
    for filename in files:
        file, ext = os.path.splitext(filename)

        if ext not in IMG_EXTENSIONS:
            continue

        for width in IMG_WIDTHS:
            size = [width, 1000]
            webp_filename = f"{file}-{width}.webp"
            webp_file_path = os.path.join(root, webp_filename)

            if not os.path.exists(webp_file_path):
                print(f"Convert {filename} to {webp_filename}")
                im = Image.open(os.path.join(root, filename)).convert("RGBA")
                im.thumbnail(size)
                im.save(webp_file_path, "WEBP")
            else:
                if DEBUG:
                    print(f"Already exists {webp_file_path}")
                

print("\nAll done!\n")
